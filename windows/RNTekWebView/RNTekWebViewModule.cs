using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Tek.Web.View.RNTekWebView
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNTekWebViewModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNTekWebViewModule"/>.
        /// </summary>
        internal RNTekWebViewModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNTekWebView";
            }
        }
    }
}
