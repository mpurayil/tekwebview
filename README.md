
# react-native-tek-web-view

## Getting started

`$ npm install react-native-tek-web-view --save`

### Mostly automatic installation

`$ react-native link react-native-tek-web-view`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-tek-web-view` and add `RNTekWebView.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNTekWebView.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNTekWebViewPackage;` to the imports at the top of the file
  - Add `new RNTekWebViewPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-tek-web-view'
  	project(':react-native-tek-web-view').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-tek-web-view/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-tek-web-view')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNTekWebView.sln` in `node_modules/react-native-tek-web-view/windows/RNTekWebView.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Tek.Web.View.RNTekWebView;` to the usings at the top of the file
  - Add `new RNTekWebViewPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNTekWebView from 'react-native-tek-web-view';

// TODO: What to do with the module?
RNTekWebView;
```
  